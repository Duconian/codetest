﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName="Brains/Player Controlled")]
public class PlayerControlledTank : TankBrain
{

	public int PlayerNumber;
	private string m_MovementAxisName;
	private string m_TurnAxisName;
	private string m_FireButton;
	private string m_SwitchButton;						//Button for switching ammo

	private TankMovement movement = null;               //Retained reference for the tank's in world movement controller
	private TankShooting shooting = null;				//Retained reference for the tank's shooting controller

	public void OnEnable()
	{
		m_MovementAxisName = "Vertical" + PlayerNumber;
		m_TurnAxisName = "Horizontal" + PlayerNumber;
		m_FireButton = "Fire" + PlayerNumber;
		m_SwitchButton = "Switch" + PlayerNumber;
	}

	public override void Think(TankThinker tank)
	{
		//Movement input
		if (movement == null)
		{
			movement = tank.GetComponent<TankMovement>(); //Finding component here so ensures the component is loaded and active in scene upon allocation.
		}
		movement.Steer(Input.GetAxis(m_MovementAxisName), Input.GetAxis(m_TurnAxisName));

		//Shooter input
		if (shooting == null)
		{
			shooting = tank.GetComponent<TankShooting>();
			//Inform ui 
			shooting.DisplayAmmoSlider();
		}

        if (shooting.CurrentAmmo > 0)
        {
			if (Input.GetButtonDown(m_FireButton))
			{
				shooting.BeginChargingShot();
			}
			else if (Input.GetButtonUp(m_FireButton))
			{
				shooting.FireChargedShot();
			}
		}

		//Switch Shells
		if (Input.GetButtonUp(m_SwitchButton))
        {
			shooting.ChangeCurrentShellType();
        }

	}
}
