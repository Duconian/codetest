﻿/// Coders :    NGD
/// Desc:       Stores the base variables and behaviours allocated to a unique shelltype
///
///

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShellType : ScriptableObject
{
    /// <summary>
    /// Prefab of the shell. Holds damage and effects data
    /// </summary>
    public Rigidbody m_Shell;

    /// <summary>
    /// The force given to the shell if the fire button is not held.
    /// </summary>
    public float m_MinLaunchForce;
    /// <summary>
    /// The force given to the shell if the fire button is held for the max charge time.
    /// </summary>
    public float m_MaxLaunchForce;
    /// <summary>
    /// How long the shell can charge for before it is fired at max force.
    /// </summary>
    public float m_MaxChargeTime;

    /// <summary>
    /// How many shots this shell type can provide before it needs to be reloaded.
    /// </summary>
    public int m_MagazineSize;
    /// <summary>
    /// How long between each shot with this shelltype. How long to chamber a round.
    /// </summary>
    public float m_ChamberTime;

    /// <summary>
    /// Unique functionality when the shell is used.
    /// </summary>
    public abstract void Fire(TankShooting tankShooting);
}
