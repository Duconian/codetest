﻿/// Coders :    NGD
/// Desc:       Stores the variables and behaviours allocated to this unique shelltype
///
///

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Shells/StandardShell")]
public class StandardShell : ShellType
{
    /// <summary>
    /// Unique functionality when the shell is used.
    /// </summary>
    public override void Fire(TankShooting tankShooting)
    {
        // Create an instance of the shell and store a reference to it's rigidbody.
        Rigidbody shellInstance =
            Instantiate(m_Shell, tankShooting.m_FireTransform.position, tankShooting.m_FireTransform.rotation) as Rigidbody;

        // Set the shell's velocity to the launch force in the fire position's forward direction.
        shellInstance.velocity = tankShooting.m_CurrentLaunchForce * tankShooting.m_FireTransform.forward;

        // Change the clip to the firing clip and play it.
        tankShooting.m_ShootingAudio.clip = tankShooting.m_FireClip;
        tankShooting.m_ShootingAudio.Play();

        // Reset the launch force.  This is a precaution in case of missing button events.
        tankShooting.m_CurrentLaunchForce = m_MinLaunchForce;
    }
}
