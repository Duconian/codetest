﻿/// Coders :    NGD
/// Desc:       Controls data and functionality related to the generation of collectables
///
///

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableManager : MonoBehaviour
{
    /// <summary>
    /// The possible shelltype replenishment options that can be spawned
    /// Possibility here to make these scriptable objects rather than prefab references
    /// </summary>
    public GameObject[] ammoCrateTypes;
    /// <summary>
    /// Possible map positions for general collectable spawning
    /// (Taking liberty for test to utilise GameManager.SpawnPoints for this)
    /// </summary>
    public Transform[] SpawnPoints;
    /// <summary>
    /// Reference to the currently occupied spawn point indexes
    /// </summary>
    private List<int> usedSpawnIndex = new List<int>();

    /// <summary>
    /// Minimum range before next collectable spawned
    /// </summary>
    private int minGenerationTime = 4;
    /// <summary>
    /// Maximum range before next collectable spawned
    /// </summary>
    private int maxGenerationTime = 8;
    /// <summary>
    /// Flag denoting state of manager loop
    /// </summary>
    private bool running = false;
    /// <summary>
    /// 
    /// </summary>
    private IEnumerator generationLoop;
    /// <summary>
    /// Controller call for beginning and ending the collectable generation from external controller
    /// </summary>
    /// <param name="shouldRun"></param>
    public void RunCollectableGeneration(bool shouldRun)
    {
        running = shouldRun;
        if (shouldRun && generationLoop == null)
        {
            generationLoop = GenerateAmmoCrates();
            StartCoroutine(generationLoop);
        }
        else if (!shouldRun)
        {
            StopCoroutine(generationLoop);
            generationLoop = null;
        }
    }

    /// <summary>
    /// Whilst game is running, spawn collectables
    /// </summary>
    /// <returns></returns>
    IEnumerator GenerateAmmoCrates()
    {
        while (running)
        {
            yield return new WaitUntil(() => usedSpawnIndex.Count != SpawnPoints.Length);

            //randomise spawn location
            int spawnIndex = Random.Range(0, SpawnPoints.Length);
            while (usedSpawnIndex.Contains(spawnIndex))
            {
                spawnIndex = Random.Range(0, SpawnPoints.Length);
            }
            usedSpawnIndex.Add(spawnIndex);
            Transform supplyDrop = SpawnPoints[spawnIndex];

            //Randomise spawn time
            int counter = Random.Range(minGenerationTime, maxGenerationTime);
            //randomise spawn type
            GameObject carePackage = ammoCrateTypes[Random.Range(0, ammoCrateTypes.Length)];

            yield return new WaitForSecondsRealtime(counter);

            // Create an instance of the collectable.
            AmmoCrate ammoCrate = Instantiate(carePackage, supplyDrop.position, supplyDrop.rotation).GetComponent<AmmoCrate>();
            //Register on collectable any appropriate collection listeners
            ammoCrate.onCollect += () => ClearUsedIndex(spawnIndex);
        }
        generationLoop = null;
    }


    public void ClearUsedIndex(int emptyingIndex)
    {
        usedSpawnIndex.Remove(emptyingIndex);
    }
}
