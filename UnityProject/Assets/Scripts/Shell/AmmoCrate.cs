﻿/// Coders :    NGD
/// Desc:       Designates the functionality provided by this collectable
///
///

using UnityEngine;
using UnityEngine.Events;

public class AmmoCrate : MonoBehaviour
{
    /// <summary>
    /// Listener for external managers for when this collectable is collected
    /// </summary>
    public UnityAction onCollect = null;

    /// <summary>
    /// Accessable ShellTypes from this collectable
    /// </summary>
    public ShellType[] m_ProvidedArsenal;
    /// <summary>
    /// Reference to this objects collider
    /// </summary>
    private Collider m_Collider;
    /// <summary>
    /// Flag for successful appropriate collision and functionality call
    /// </summary>
    private bool m_Collected = false;


    private void OnCollisionEnter(Collision collision)
    {
        if (!m_Collider)
        {
            m_Collider = GetComponent<Collider>();
        }

        // On Enter will reduce call for any ground collision unless rigid body and shell force applied
        // Collect all the colliders in a sphere from the current position to the collectables extent (assuming x=y). 
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_Collider.bounds.extents.x*1.5f);

        // Go through all the colliders...
        for (int i = 0; i < colliders.Length; i++)
        {
            // ... and find any TankShooting components.
            TankShooting tankShooting = colliders[i].GetComponent<TankShooting>();

            //if tank collected and another hasnt already been registered to have collected this
            if (tankShooting && !m_Collected)
            {
                // Update the tanks arsenal.
                tankShooting.ResupplyArsenal(m_ProvidedArsenal);

                m_Collected = true;
            }
        }

        //If a successful appropriate collision was encountered
        if (m_Collected)
        {
            //Trigger and clear listeners
            onCollect?.Invoke();
            onCollect = null;
            // Destroy this collectable.
            Destroy(gameObject);
        }
    }
}
