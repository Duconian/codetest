﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class TankShooting : MonoBehaviour
{    
    public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
    public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
        
    public Slider m_AmmoSlider;                             // The slider to represent how much ammo the tank currently has.
    public Image m_AmmoFillImage;                           // The image component of the slider.
    public Color m_FullAmmoColour = Color.green;            // The colour the ammo bar will be when has full ammo.
    public Color m_ZeroAmmoColour = Color.red;              // The colour the ammo bar will be when has no ammo.
    private bool showingAmmoUi = false;                     // Flag denoting if this tank uses finite ammunition and thus needs visual feedback. Default false assuming AI tank

    private ShellType m_currentShellType;        // The ammunition currently being used by the tank
    private Dictionary<ShellType, int> m_availableShellTypes = new Dictionary<ShellType, int>();    // The selection of available ammunition types this tank can select from and the available amount currently carried

    public float m_CurrentLaunchForce;          // The force that will be given to the shell when the fire button is released.
    private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.
    private bool m_Charging;

    private bool m_Rechambering;                // Is a new shot being prepared
    private float m_RechamberTime;              // Current duration of preparing for an additional shot 

	public bool IsCharging
	{
		get { return m_Charging; }
	}
	
    private void OnEnable()
    {
        //InitStartingShell(new StandardShell());
    }

    private void Start ()
    {
        
    }

    /// <summary>
    /// Defaults the beginning shell
    /// </summary>
    /// <param name="shell"></param>
    private void InitStartingShell(ShellType shell)
    {
        // Designate default
        m_currentShellType = shell;
        // When the tank is turned on, reset the launch force and the UI
        m_CurrentLaunchForce = shell.m_MinLaunchForce;
        m_AimSlider.value = shell.m_MinLaunchForce;

        // The rate that the launch force charges up is the range of possible forces by the max charge time.
        m_ChargeSpeed = (m_currentShellType.m_MaxLaunchForce - m_currentShellType.m_MinLaunchForce) / m_currentShellType.m_MaxChargeTime;

        // Update ui
        SetAmmoMaxVisuals();
        SetAmmoVisuals();
    }

    public void BeginChargingShot()
	{
		if (m_Charging || m_Rechambering) return;

		m_CurrentLaunchForce = m_currentShellType.m_MinLaunchForce;

		// Change the clip to the charging clip and start it playing.
		m_ShootingAudio.clip = m_ChargingClip;
		m_ShootingAudio.Play();

		m_Charging = true;
	}

	public void FireChargedShot()
	{
		if (!m_Charging) return;

        //Shelltype handles firing and results
        m_currentShellType.Fire(this);
        //Update shelltype logistics
        CurrentAmmo--;
        m_Charging = false;
        
        //Account for preparing new shot
        m_RechamberTime = 0;
        m_Rechambering = true;
	}

    /// <summary>
    /// Handler for the shelltype ammo amount currently in use
    /// </summary>
    public int CurrentAmmo
    {
        get 
        {
            return m_availableShellTypes[m_currentShellType];
        }
        set 
        {
            //Hardcode above and at 0 to avoid UI issues
            if (value >= 0)
            {
                m_availableShellTypes[m_currentShellType] = value;
                //update ui
                SetAmmoVisuals();
            }
        }
    }

    public void ChangeCurrentShellType()
    {
        // If too little to warrant changing ammo types, skip
        if (m_availableShellTypes.Count < 2)
        {
            return;
        }

        //Find current index to progress from (can be made more performant) 
        int currentIndex = m_availableShellTypes.Keys.ToList().IndexOf(m_currentShellType) + 1;

        if (currentIndex >= m_availableShellTypes.Keys.Count)
        {
            // Loop list if beyond dictionary bounds
            currentIndex = 0;
        }

        //Update in use ShellType
        m_currentShellType = m_availableShellTypes.ElementAt(currentIndex).Key;
        //Update new scales for the UI
        SetAmmoMaxVisuals();
        //Update ammo and UI
        CurrentAmmo = m_availableShellTypes[m_currentShellType];
    }

    /// <summary>
    /// Instanciates the available shell types. Defaults to full armament from round start
    /// </summary>
    /// <param name="providedArsenal"></param>
    public void SetUpArsenal(ShellType[] providedArsenal)
    {
        for (int i = 0; i < providedArsenal.Length; i++)
        {
            m_availableShellTypes.Add(providedArsenal[i], providedArsenal[i].m_MagazineSize);

            if (providedArsenal[i] is StandardShell)
            {
                //update default
                InitStartingShell(providedArsenal[i]);
            }
        }
        //update ui
        SetAmmoVisuals();
    }

    /// <summary>
    /// Restocks the ammo count of provided shelltype(s)
    /// </summary>
    /// <param name="providedResupply"></param>
    public void ResupplyArsenal(ShellType[] providedResupply)
    {
        for (int i = 0; i < providedResupply.Length; i++)
        {
            if (m_availableShellTypes.ContainsKey(providedResupply[i]))
            {
                m_availableShellTypes[providedResupply[i]] = providedResupply[i].m_MagazineSize;
            }
        }
        //update ui
        SetAmmoVisuals();
    }

    private void Update()
	{
		if (m_Charging)
		{
			m_CurrentLaunchForce = Mathf.Min(m_currentShellType.m_MaxLaunchForce, m_CurrentLaunchForce + m_ChargeSpeed*Time.deltaTime);
			m_AimSlider.value = m_CurrentLaunchForce;
		}
		else
		{
			m_AimSlider.value = m_currentShellType.m_MinLaunchForce;
		}

        if (m_Rechambering)
        {
            m_RechamberTime += Time.deltaTime;
            if (m_RechamberTime >= m_currentShellType.m_ChamberTime)
            {
                m_Rechambering = false;
            }
        }
	}

    #region Ammunition Visuals
    /// <summary>
    /// Caller for Player controlled tanks to display their ammunition
    /// (For test, AI tanks dont have finite ammo thus no need for visuals.
    /// Prefabs all assumed to have visuals disabled until)
    /// </summary>
    public void DisplayAmmoSlider()
    {
        m_AmmoSlider.gameObject.SetActive(true);
        showingAmmoUi = true;
    }

    /// <summary>
    /// Reads the current used ammo type and amends slider so accounts for variable max limits affecting visuals
    /// </summary>
    public void SetAmmoMaxVisuals()
    {
        // Set the slider's value appropriately.
        m_AmmoSlider.maxValue = m_currentShellType.m_MagazineSize;
        //update the aim UI
        m_AimSlider.minValue = m_currentShellType.m_MinLaunchForce;
        m_AimSlider.maxValue = m_currentShellType.m_MaxLaunchForce;
    }

    /// <summary>
    /// Reads the current used ammo type and visually represents it
    /// </summary>
    public void SetAmmoVisuals()
    {
        // Set the slider's value appropriately.
        m_AmmoSlider.value = CurrentAmmo;

        // Interpolate the colour of the bar between the choosen colours based on the current percentage of the starting ammo.
        m_AmmoFillImage.color = Color.Lerp(m_ZeroAmmoColour, m_FullAmmoColour, CurrentAmmo / m_currentShellType.m_MagazineSize);
    }
    #endregion
}